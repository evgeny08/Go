package main

import (
	"fmt"
	"os"
	"strconv"
	"gopl.io/ch2/lengthconv"
)

func main() {
	for _, arg := range os.Args[1:] {
		t, err := strconv.ParseFloat(arg, 64)
		if err != nil {
			fmt.Fprintf(os.Stderr, "cf: %v\n", err)
			os.Exit(1)
		}
		in := lengthconv.Inch(t)
		cm := lengthconv.Centimeter(t)
		fmt.Printf("%s = %s, %s = %s\n",
			in, lengthconv.Inch(in), cm, lengthconv.Centimeter(cm))
	}
}

