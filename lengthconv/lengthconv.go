package lengthconv

import "fmt"

type Inch float64
type Centimeter float64

func (in Inch) String() string    { return fmt.Sprintf("%g°In", in) }
func (cm Centimeter) String() string { return fmt.Sprintf("%g°Cm", cm) }


